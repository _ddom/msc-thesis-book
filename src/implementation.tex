% This chapter assumes that the CHABADA technique has been explained at a theoretical level

\chapter{Running CHABADA against the Tacyt dataset\label{sec:implemenation}}

This chapter presents first the limitations of the existing implementation of the CHABADA technique. Once the limitations have been stated, it presents an explanation of the changes that are needed in the implementation in order to improve the scalability of the implementation and a brief description of the technologies stack. The next section would then explain in detail the implementation, once both the design and the technologies have been stated. This chapter concludes with a description of the known limitations of this new design and architecture, either old limitations that could not be addressed with the new design or new limitations introduced in the new architecture.

\section{Limitations of the old implementation\label{sec:legacycode}}

The starting point of the design and development is an existing implementation of a data pipeline using Luigi (\ref{sec:luigi}). This pipeline implements each step as a Task. Each Task has a set of requirements and a set of outputs. In order to run a certain Task, the outputs must be unsatisfied and the requirements satisfied.

The first step in the old pipeline is the preprocessing of the plain text descriptions. This needs as input a set of natural language descriptions in plan text format. The Task reads each description, process it and store the result in one file per application. From that point the pipeline executes a topic modeling tool called Mallet. This tool extracts the topics from the input text and assigns scores for each topic to each application. From this point, the results are processed in a Task that implements the Kmeans clustering algorithm. Once the clusters are created, the pipeline expects a mapping between the applications and its permissions. With this information it will apply the KNN algorithm to find outliers from the clusters.

This implementation has several flaws that have been addressed. The first problem is that storing everything in files is costly when the data is in the order of hundreds of thousands. Along with those levels of input size, the implementation is not prepared to scale horizontally in order to be able to ingest all the input at a reasonable peace.

The old code base also used the permission list for applying KNN, however, using only the permissions list is not enough because there isn't always a one-to-one mapping of the declared permissions to actually used permissions~\cite{Stevens:Permission:MSR2013}.

\section{The new architecture\label{sec:new_arch}}

In the legacy implementation the data was stored in files in the file system. Each piece of data of an application was in a separate file identified by the package name in the name of the file. While this approach is quick and simple; reading and writing files in the order of hundreds of thousands would be stressful to the file system and could even help reduce the lifetime of the drive.

The old implementation already had a pipeline style architecture that has been adapted to the new architecture. The main idea is to implement the tasks as atomic as possible to allow fine control of the execution. This will allow the system to control the parallelism more easily. In the old design each task took a piece of data as input and produced some other piece of data.

The pipeline and files approach is interesting to be maintained, but the persistence must be implemented in something that is designed for scalability. This performant persistence component must be able to handle several operations simultaneously since the tasks can potentially be executed in parallel to each other. The component that could fit best would be an eventually persistent database with locking at the row level.

The tasks would be chained to its dependencies, like in the old design. In this iteration however, each task operates over N rows of data, being N a configurable number. In the old implementation each task would operate over all the elements of the dataset in one thread. With those N-ary subsets of the input, each task would declare its dependencies to operate over the same subset. With this the tasks chains are completely independent from one another. Figure~\ref{logical_arch} explains graphically the new design and its main components.

\begin{figure}[h!]
  \includegraphics[width=\textwidth]{../graphics/logical_architecture}  
  \caption{The new design and its logical components}
  \label{logical_arch}
\end{figure}

For the permissions usage part, the goal would be to obtain information from the application binary. A static analyzer can obtain a list of android related API calls. This list of API calls can then be filtered to have only the API calls related to permissions. It's not interesting to have in the set API calls that are common to all android applications, like the application life cycle API calls, that only introduce noise in the dataset.


\section{Technologies Stack\label{sec:tech_stack}}

\subsection{The Luigi framework\label{sec:luigi}}

Luigi is a free software framework for creating pipelines of dependencies, and it is implemented by Spotify. The programmer builds small tasks that perform a certain action. If a task depends of the output of another one, the programmer simply declares this dependency, and Luigi automatically runs the dependencies prior to the task to ensure that the dependencies are met before running the task. The framework is built for the Python programming language.

The framework has batteries included. The framework includes a lot of modules for running tasks in certain execution environments like PySpark or Hadoop. It also includes a lot of modules for specifying dependencies on external systems like HDFS or several databases. Although most of the database modules are maintained by the community. Creating custom modules for this kind of operations is also easy thanks to a simple class model. To create a custom element, no matter if its a task or a dependency module the programmer only has to inherit the base class for that kind of thing and implement in the template methods the custom behavior of the component. Figure~\ref{how_to_make_a_luigi_task} (from the official documentation) explains how this works.

\begin{figure}[h!]
  \includegraphics[width=\textwidth]{../graphics/task_breakdown}
  \caption{How a Luigi task is implemented. Taken from the Luigi official documentation~\cite{TasksLu63:online}}
  \label{how_to_make_a_luigi_task}
\end{figure}

Luigi also support concurrent execution of tasks given that the dependency graph continues to be sound (no task is run with unmet dependencies). For example, if one task has several other dependencies, Luigi will fill all the available workers with dependent tasks to be run and will be run in parallel. Once all this dependencies have been run, the initial task will be run. Luigi will also avoid rerunning tasks during execution. The output of a task is first check and the output is found then the task is not run. There's also the scenario in which two tasks an have the same exact dependency with the same parameters and configuration, instead of running it twice, Luigi will cache the semantic information of the task and will run only one instance of that particular task.

\begin{figure}[h!]
  \includegraphics[width=\textwidth]{../graphics/luigid_gui}
  \caption{The status of the running tasks can be tracked with the remote scheduler web panel}
  \label{luigi_scheduler}
\end{figure}

To handle all the task management Luigi has a scheduler that is designed to be distributed by default. The remote scheduler (Figure~\ref{luigi_scheduler}) is a web application where workers submit tasks that need to be run. This scheduler only tracks the executions, but the actual running of the code is done by the worker that submitted the task. However, once a task is submitted to the scheduler it will be enqueued. When the task is ready to be run, it will be sent to the worker in order to be executed. Luigi also has dumb workers that will take tasks from other workers when the scheduler says so. This load balancing is primitive, but this is because a good load balancer is not in the project goals.

\subsection{MongoDB\label{sec:mongodb}}

MongoDB is a No-SQL document database. The main features of MongoDB is that it is schema free and that is designed to be fast. It easily scales by creating clusters and it is able to handle a lot of simultaneous connections at the same time. While writing, the locking mechanism locks the document, contrary to traditional SQL databases that lock the entire table by default. One downside of MongoDB is that the whole database is locked during maintenance, but this is not an issue for my use case, because the system only needs to run during experiments and evaluations. A downtime for maintenance is permitted. Other interesting feature of MongoDB is indexing, which dramatically increases the reading times.

\subsection{Docker\label{sec:docker}}

Docker is a container system that allows the creation of containerized applications. Usually this is meant for distribution in cloud environments. The usage of Docker is to create a container with a certain application fully installed. Then, this container can be instantiated all the times necessary and each instance of the application will be isolated from the rest. The containers can be connected to other containers, the host system and the Internet to allow distributed capabilities in the encapsulated applications. The most typical use case is to deploy a complete stack of a certain web application and to submit the container to cloud platforms that run the application. This application can then be instantiated several times to allow load balancing and other distributed features transparently.

The new architecture has several elements that need to be communicated with each other. By using Docker containers the deployment of the whole system is simplified.

\section{Implemented changes\label{sec:changes}}

\subsection{Data storage}

MongoDB has the features described in the architecture section, so I have used it to implement the persistence layer in the pipeline. The type of data that I am dealing with exploits also the schema free capabilities of MongoDB because not all fields are available for each application inside Tacyt.

In the MongoDB database each document is identified by an unique identifier. By default this identifier is automatically generated by the system. However, this behavior is not interesting for my need because the input for the system is a list of android application packages. This can be changed though, but there are certain rules. The most significant one for my use case is that dots are not allowed in the name. This means that Java style package names, with dots separating each namespace can't be used. The system internally should have to modify the names of the applications to an alternative that is unique too and is allowed in the id rules of MongoDB.\@ In the Java style naming convention slashes are not allowed, however, they are in MongoDB identifier rules. This means that I will transform each dot in the package name with a slash. For example, \code{com.whatsapp} would become \code{com/whatsapp}. Is easy to see intuitively that this transformation is bijective in both directions and both transformations satisfy $(f \circ f)(x) = f(x)$. This will allow to uniquely identify an application in any part of the system by simply applying the necessary transformation.

In the MongoDB database I would need to store several fields related to a certain application. This fields are the following:

\begin{itemize}
\item \code{versions}: This field will store all the available versions found in the Tacyt dataset.
\item \code{latest}: This field will store the latest version that is in the \code{versions} list.
\item \code{filtered\_descr}: This field will store the cleaned up natural language description of the application. The source of this description is the description field found in the \code{latest} field.
\item \code{topics}: This field stores the results of the topic modeling operation. Each application has a set of probabilities of belonging to a certain topic.
\end{itemize}

\subsection{Refactoring}

The old implementation of the filtering step was a set of functions that where connected manually by a sequence of call-n-store steps as seen in Figure~\ref{old_filtering_pseudocode}.

\begin{figure}[h!]
  \centering
\begin{BVerbatim}
text = "..."
text = step1(text)
text = step2(text)
...
text = stepN(text)
\end{BVerbatim}
\caption{Pseudo-code of the old implementation approach}
  \label{old_filtering_pseudocode}
\end{figure}

A cleaner approach is to implement the filtering steps as an instance of the decorator pattern. This pattern takes an interface and implements some code that complements or overrides the implementation of a delegate member of the same interface. This pattern is great for creating this kind of sequential operations that progressively modify some piece of information.

With this implementation the steps are connected to the other ones by the interface and by being encapsulated in classes that can maintain an internal state between invocations. This is interesting because some steps require resources like dictionaries or algorithms that can be expensive to instantiate each time a piece of text is passed through.

To aid in the creating of the decorator chain, a builder class serves as a public API that internally connects the decorators by some methods. Figure~\ref{decorator_uml} describes in UML the design of the classes.

\begin{figure}[h!]
  \includegraphics[width=\textwidth]{../graphics/decorator_uml}
  \caption{This UML diagram shows the design used during the refactoring of the filtering task}
  \label{decorator_uml}
\end{figure}

\subsection{Scalability and load balancing}

As mentioned in the Luigi section (\ref{sec:luigi}), Luigi doesn't implement automatic load balancing but allows the parallel execution of tasks. In order to be able to ingest the whole dataset in a timely manner the legacy implementation would need to be changed. In the old implementation each task took all the input and processed it sequentially.

Most of the operations in the system could be parallelized because those operation where handling data at the row level and could be parallelized to that level. However, the cost of switching context in the tasks could lead to a performance loss if the cost of loading a task is greater that the cost of processing a row. To avoid this costs if they occur, the parallelism was designed with chunks of data in mind. Using the old implementation of a processing loop as base, the tasks could be reimplemented to use a piece of data, represented in a list. With this model the load can be balanced by setting the chunk size.

\subsection{Communication with Tacyt\label{sec:tacyt}}

In order to be able to obtain data from Tacyt and feed it to the pipeline, a module must be implemented that takes data from Tacyt and stores it in the database. However, not all applications in Tacyt are valid. Some have natural language descriptions in languages different than English or mixed with it. Also, there wasn't a list of packages of the applications that where in the dataset and the application binary wasn't easily available for downloading from the API.\@ To overcome this problem, I decided to use the Androzoo dataset for downloading the application binaries.

Before running the pipeline with the dataset I need to clean it up. As a starting point I used the CISPA dataset, which is a list of 1.6 million app package names. To create a cleaned dataset using this list I would need to filter out the applications that don't follow this criteria:

\begin{itemize}
  \item Tacyt has the application in the dataset.
  \item The application record has all the necessary information.
  \item Either the description or the translated description found in Tacyt is in English.
  \item There was an available application binary hash that could be used to reference it in Androzoo.
\end{itemize}

To filter the applications, I implemented a small program that downloaded application metadata and applied all the filtering steps. If one application was filtered out by some of the filters it was stored in the database with a \code{status} field that contains information about what filtering step decided to throw it. If an application survives all the filtering steps it means that it is eligible for the final dataset. In this case it was saved in the database with the corresponding \code{status} field stating it.

To description language filtering step was done with a pretrained dataset found in the Python package \code{langid}. This package exported a function that returns the probability of belonging to a certain language. If the description was above a certain threshold and the language was English, then the application was considered good for the next filtering step. In the execution the threshold value was $60\%$.

\subsection{Static analysis}

% Old
% The old implementation used \emph{smali} and \emph{apktool} to do the static analysis. This tools generate an intermediate representation of the bytecode that can be parsed. In this version of CHABADA I used \emph{soot} to do the analysis. \emph{soot} parses in memory the application and exposes a Java API that can be used to query the system. With this framework I avoid two things; writing to much intermediate files to the filesystem and writing parsers. The analysis program is fairly simple, it looks for each class in the application code and searches for all method invocations, then filters out the methods that are not method invocations to the Android framework and that are not sensitive API calls. This leave me with the sensitive API calls made by the application code.

The old implementation used \emph{smali} and \emph{apktool} to do the static analysis. This tools generate an intermediate representation of the bytecode that can be parsed. In this version of CHABADA I used \emph{androguard} to do the analysis. \emph{androguard} parses in memory the application and exposes a Python API that can be used to query the apk file. With this framework I avoid two things; writing to much intermediate files to the filesystem and writing parsers. The analysis program is fairly simple, it looks for each class in the application code and searches for all method invocations, then filters out the methods that are not method invocations to the Android framework and that are not sensitive API calls. This leave me with the sensitive API calls made by the application code. The static analyzer has been developed as a separate module and can be found in Github \cite{dex2call_github}.


\subsection{Modules}

The pipeline is composed by a set of tasks that each implement a certain action in the pipeline. In Figure~\ref{modules_arch} can be seen the dependencies of each task.

\begin{figure}[h!]
  \includegraphics[width=\textwidth]{../graphics/tasks_dependencies}  
  \caption{The concrete architecture of the pipeline with each task and its dependencies}
\label{modules_arch}
\end{figure}

\begin{itemize}[label={}]
\item \code{TacytAppMetadataFromList}: This task takes a list of package names and downloads from Tacyt the metadata found for each version of the application. All the data is stored as is in the \code{versions} field of the app's document.
\item \code{TacytAppMetadataFromFile}: This task allows the easy running of \code{Tacyt\-App\-Metadata\-From\-List} by reading a file with package names and creating tasks that actually download the metadata. It serves nothing but as a facade.
\item \code{FindLatestVersionFromList}: This task will take the result of \code{Tacyt\-App\-Metadata\-From\-List} and will search for the latest version the application has. The latest version will be copied as is in the \code{latest} field in the app's document.
\item \code{FindLatestVersionFromFile}: Similar to \code{TacytAppMetadataFromFile}, this task will read a file with package names and will invoke \code{FindLatestVersionFromList} with subsets of the input depending on the chunk size.
\item \code{FiltersFromList}: This task will take the natural language description of the application from the \code{latest} field in the app's document. This text is processes by a series of filters and then it stores the cleaned description in the \code{filtered\_descr} field.
\item \code{FiltersFromFile}: This task will serve as a facade to \code{FiltersFromList} by reading a file with package names and running instances of that task with the obtained chunks.
\item \code{ConvertFilteredDescriptionToTXTFromList}: Since I didn't changed the old Mallet component, I still need to store the descriptions as text files in order to be able to use it as input for Mallet. This task will take as input the result of  \code{FiltersFromList} and store each description in a text file.
\item \code{ConvertFilteredDescriptionToTXTFromFile}: This task will read a file with package names and split them in chunks. Each chunk will be handled by an instance of \code{ConvertFilteredDescriptionToTXTFromList}. This task will serve as an entry point of the cleanup phase and the mallet tasks will use it to launch said phase.
\item \code{RunMallet}: This task takes the files created by the \code{Convert\-Filtered\-Description\-To\-TXT\-From\-List} tasks and feed it to a mallet instance that is executed as an external program. This program will produce an output in the specified folder.
\item \code{MalletChunk}: This task declares \code{RunMallet} as a dependency and will read the output of this task and write it in MongoDB.\@ It is designed to be parallel. Each instance will read the file, take the applications that belongs to it's chunk and store it.
\item \code{Mallet}: This task will take a file with package names and will create \code{MalletChunk} dependencies. This task is the entry point for the topic modeling phase.
\item \code{KmeansTraining}: This task will read the input of the \code{Mallet} task and will perform clustering using the Kmeans algorithm. It can also optionally do the silhouette of the cluster to check if the cluster parameters are fit to the type of data used.
\item \code{DownloadListOfAPKS}: This task will download from the Androzoo repository each application specified. The information needed for downloading is taken from the \code{FindLatestVersionFromList} task.
\item \code{DownloadListOfAPKSFromFile}: As all the other \code{*FromFile} tasks, this one takes a file with package names and sends chunks of package names to instances of \code{DownloadListOfAPKS}.
\item \code{ExtractAPICallsFromList}: This task will take the APK files downloaded in \code{DownloadListOfAPKS} and apply a simple static analysis that extracts the API calls related to the Android framework. This list is then filtered using a mapping between API calls and permissions.
\item \code{ExtractAPICallsFromFile}: This task reads a file of package names and sends chunks of package names to instances of \code{ExtractAPICallsFromList} for analysis of their related APKs.
\item \code{BuildClusterFile}: This task will augment the information of each application in the clusters with the information about what API calls each application is using.
\item \code{KNN}: Using the results of \code{BuildClusterFile} this task will apply the KNN algorithm to the clusters for searching outliers in them.
\end{itemize}

\subsection{Deployment}

Using Docker as the deployment solution I deployed the pipeline in a virtual server managed by the IMDEA Software Institute with 72 cores and 62GB of RAM.\@ Since it was a shared server with other researchers of the institute I used only about 20 cores of the ones available.

There are 3 main components to be deployed; a Luigi scheduler, a MongoDB instance and a worker with the pipeline implementation. The two first components where implemented each in a container and launched in the server. The worker component has one container instance for each worker in the system. When the pipeline has to be launched, one container with the needed parameters is launched and connected to the scheduler and database containers. This worker containers can be instantiated several times given that the connections to the outside of the container are not overlapping (i.e: they are not writing to the same directory and files).

\section{Known limitations of the new architecture\label{sec:current_limitations}}

There are two known important limitations of the new architecture. Both of them are related to the size of the data being used. The first limitation is that PyMongo, the python library used to connect to MongoDB has a limit in the amount of bytes that can be sent to the database. This limit is enforced by the library and is caped at 16MB.\@ This 16MB limit is famous in MongoDB but is only enforced by the database in the document size. MongoDB does not have a limit in the size of the data that is being transmitted. PyMongo however enforces a limit of 16MB when sending queries to the database. This force me to split the storage of the Mallet results in several tasks that only would take chunks of the result file. PyMongo didn't allow to send the whole insert query at once because the query was larger than this limit.

%% Old silhouette bug
% The second limitation is due to a bug in sklearn. The module that implemented the Kmeans algorithm was only modified to use MongoDB as input. The rest of the code was left unchanged. The last part of the implementation calculates the silhouette of the cluster. In the previous iterations of CHABADA calculating the silhouette wasn't a problem. The current implementation of the silhouette makes a Cartesian product of the elements in the cluster. This means that for a large dataset like mine, the memory consumption was too high. Even with the resources available to run the pipeline the interpreter entered in an unrecoverable Out Of Memory condition that crashed the system. As I will explain in the results chapter, this caused that I was not able to calculate the silhouette of the clusters to find what combination of number of topics and number of clusters was the most appropriate one.

%% New limitations with the silhouette
The evaluation of the clusters using the silhouette of them has some limitations due to the large size of the dataset. The Silhouette works by performing a Cartesian products of all the elements in the space and stores it in a matrix, this means that with a dataset of more than 700k applications, the matrix will be square that. Sklearn has an implementation of the silhouette that is can handle in memory such large datasets, but as a drawback, the implementation is painfully slow. This can be a very limiting problem. However, the silhouette implementation allows to sample the dataset and calculate the silhoutte on that sample. This means that the algorithm works with an amount of data that can feasibly handle with the drawback that is losing precision. For CHABADA I decided to do a sampling of a 25\% of the dataset, this size fits in the computer's memory and can be finished in minutes.

%% XXX: If I still have the heatmap of the standard deviations I will drop it here to justify that the sampling is sound. If not, I will just not mention it because is a pain in the ass to generate.
%% XXX: I still have it, but is only 10%

Since the sampling involves a random process, I don't want to take the risk of losing the big picture of the data with the sampling by taking a non representative sample. To make sure the sampling was not screwing with my results I ran a 10\% sampling 30 times each pair and then calculated the standard deviation. As Figure \ref{std_dev_heatmap}, the deviation is not sufficiently large to suppose that the samples differ too much from one another.

\begin{figure}[h!]
  %% TODO: Crear photo ready para los resultados del stddev.
  \includegraphics[scale=0.45]{graphics/sil_stddev}
\caption{The standard deviation of sampling at 10\%.}
\label{std_dev_heatmap}
\end{figure}

\section{Ground truth for the Machine Learning algorithms}

Although I already have the silhouette algorithm for evaluating the fitness of the hyperparameters to the data, I wanted to see how fit where the hyperparameters to some real world classification of applications. Since I am trying to evaluate the fitness of how related some applications are, I used some information from the Google Play Store. More precisely, I took the recommend, or similar, applications that the Google Play store offers to the users about an app. For example, for Whatsapp, Google Play suggest as similar applications like Telegram or LINE. It is uncertain how Google decides the similarity, but it can be used as ground truth for what the related applications are expected to be.

I built a simple scrapper that took all the applications in my dataset and got the similar applications that the Google Play store suggests. In the results there are applications that are inside my dataset and others that are outside. From this, I built a graph of applications by their similarity. The total graph has 10682425 edges from which 2102355 are edges that connect two applications in the dataset.

I compared the graph to the clusters generated  by Kmeans and used a simple score system that gives an score to a pair of apps based on their position in the graph and the clusters (Table \ref{score-table}).

\begin{table}[h!]
  \centering
\begin{tabular}{l|ll}
                           & \textbf{Connected} & \textbf{Not connected} \\ \hline
\textbf{Same cluster}      & 1                  & -0.5                    \\
\textbf{Different cluster} & -1                 & 0.5                  
\end{tabular}
\caption{The scoring system for evaluating the clusters using the graph.}
\label{score-table}
\end{table}

The idea behind the scores is to reward clusters that have applications that I know are related and to punish clusters that disconnect applications I know are related. In the case of two applications not being connected in the graph doesn't mean that they are 100\% unrelated, but that I don't know if they are or not. Hence the lower score in that case. Once each pair if applications has it's score I take the average of it and that's the silhouette of the clusters according to the graph of applications extracted from the Google Play store.

%  LocalWords:  CHABADA Tacyt dataset scalability preprocessing KNN
%  LocalWords:  Kmeans outliers performant ary Spotify TODO PySpark
%  LocalWords:  Hadoop HDFS enqueued balancer MongoDB namespace descr
%  LocalWords:  whatsapp bijective Refactoring stepN refactoring TXT
%  LocalWords:  parallelized reimplemented Androzoo CISPA metadata
%  LocalWords:  pretrained langid TacytAppMetadataFromList app's APK
%  LocalWords:  TacytAppMetadataFromFile FindLatestVersionFromList
%  LocalWords:  FindLatestVersionFromFile FiltersFromList RunMallet
%  LocalWords:  FiltersFromFile MalletChunk KmeansTraining FromFile
%  LocalWords:  ConvertFilteredDescriptionToTXTFromList APKs IMDEA
%  LocalWords:  ConvertFilteredDescriptionToTXTFromFile PyMongo
%  LocalWords:  DownloadListOfAPKS DownloadListOfAPKSFromFile sklearn
%  LocalWords:  ExtractAPICallsFromList ExtractAPICallsFromFile
%  LocalWords:  BuildClusterFile
