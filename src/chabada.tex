\chapter{The CHABADA technique\label{sec:chabada}}

In this chapter I explain the CHABADA technique in detail. This technique along with its already existing implementation is the starting point for the rest of the work presented in this thesis.

\medskip

The rationale behind this technique is based on the idea that what is considered a malicious feature for an application is actually a benign feature for other type of applications. In the Android ecosystem, which is where this technique has been developed on and evaluated against, applications are encapsulated in a sandbox where certain features of the operating system are unavailable. To access this features the application must ask for permission of the user. From this idea of requesting permissions we can get an idea of what certain type of applications should access and what not. However, this idea is not practical, because one would have to think of all possible features and properties that a program must have and decide if said features and properties fall into that category of programs or in another.

The Android application ecosystem is large, with millions of applications in the most common market, Google Play. With this applications we could take a group of related applications, for example, map and GPS applications, and find what makes them related. Once this relationship is made, we can search for applications that fall out of what is normal for this type of applications. Following the map and GPS applications example, it would be normal for this kind of application to access features like the GPS system and internet connection. It would be weird for these types of applications, instead, to request the capability of reading the phone contacts.\@

The CHABADA technique goes a step further, and tries to identify if a program acts as advertised or not. In our previous example we said that an uncommon feature of GPS apps is to read the contacts, but, what if a certain app wants to wed out its competitors by offering some social features that require those contacts to work properly. This application, although is a legitimate one, could be erroneously classified as malicious, because it offers more features than what is considered common.

To identify what is advertised in an application, the CHABADA technique takes what the users are going to receive as an advertisement of the application. More precisely, it uses the description that accompanies the app in the store page where the user can download said application. From the descriptions the technique finds the different topics that the descriptions contain. GPS application's description will talk about maps, GPS and so on. While mobile games, for example, will talk about the features of the game.

From a set of applications, the technique finds the topics and relates each application to one or more topics. From this relationship we can inspect what features are common, not for the group of applications, but for that topic. That GPS application that included social features and accessed the contacts list would not be considered malicious if those features were advertised in the application's description. 

\section{The steps of the technique}

The CHABADA technique is a sequence of steps that starts with a collection of applications and finishes with a set of clusters of applications that can be used to query for outliers of the clusters. As already mentioned, an outlier is an application that has some features that are not common for the cluster.

%% TODO: Omnipresent steps diagram from the original paper

The first step of the technique consists in a cleanup of the application's descriptions. This cleanup improves the following step; in this second step the technique applies a NLP technique known as topic modeling. This technique takes a collection of documents and extracts what topics are discussed in those documents. The topics are usually represented by the most representative words in the topic. From the topics and the data that say to what topics a document belongs to, the technique generates clusters of applications. The next step is to augment the information in this clusters by analyzing the features of the application. With this information in the clusters the technique can detect the ``interesting'' applications by using outlier detection algorithms.

\subsection{Input preprocessing}

The descriptions in the Google Play store can have a lot of variation on how they are written, what metadata, such as URLs, they contain, they can be written in several languages, etc. In order to improve the subsequent machine learning steps there must be some normalization of the data. The first thing is that NLP algorithms must be tuned for the nuances and quirkiness of each language. A model trained for the Spanish language will not work very well on Italian, and will surely not work at all on languages like Chinese. For this reason, the technique only works with one language at a time.

In the original paper and in the following chapters, we chose to work with English since is the most common language for writing descriptions. All the applications that have a description in a language that is not English are removed from the dataset. Google Play provides a translated description. It is responsibility of the implementation to properly choose the correct version of the translation if there's the case that the English version is available through this translated description.

In the Google Play store descriptions are interpreted as HTML files with the possibility of including limited HTML features like text formatting or anchors. This HTML tags need to be removed from the description because they don't include any relevant information from the point of view of the NLP algorithms. The next information that is removed from the description is numbers,  punctuation and stop words (\emph{the}, \emph{is}, \emph{at}, \emph{which}, \emph{on}, \ldots).

The final step of the cleanup is to reduce the words to their root forms. This way, similar words are reduced to the same root. To achieve this a common NLP technique for this is Stemming. This technique takes words like \emph{playing}, \emph{player} and \emph{play} and reduces them to the root \emph{plai}. After Stemming, the words that where reduced to one character words are removed because they have lost their semantics and make no sense to maintain them.

In the original paper the application \textit{\textbf{London Restaurants Bars \& Pubs +}} was used as an example. Figure~\ref{before_and_after_gym} shows the result of applying the steps to the description of this application.

\begin{figure}[h!]
  \begin{minipage}{1.0\textwidth}
    Looking for a restaurant, a bar, a pub or just to have fun in London? Search no more! This application has all the information you need:

    \begin{itemize}
    \item You can search for every type of food you want: french, british, chinese, indian, etc.
    \item You can use it if you are in a car, on a bicycle or walking
    \item You can view all objectives on the map
    \item You can search objectives
    \item You can view objectives near you
    \item You can view directions (visual route, distance and duration)
    \item You can use it with Street View
    \item You can use it with Navigation
    \end{itemize}

    Keywords: london, restaurants, bars, pubs, food, breakfast, lunch, dinner, meal, eat, supper, street view, navigation
    \caption{The description as is shown in the Google Play.}
  \end{minipage}
  \\[0.5cm]
  \begin{minipage}{1.0\textwidth}
    look restaur bar pub just fun london search applic inform need can search everi type food want french british chines indian etc can un car bicycl walk can view object map can search object can view object near can view direct visual rout distanc durat can us street view can us navig keyword london restaur bar pub food breakfast lunch dinner meal eat supper street view navig
    \caption{The description as the topic modeling algorithm will see it.}
  \end{minipage}
  \caption{The description of \textit{\textbf{London Restaurants Bars \& Pubs +}} before and after the cleaning step.}
  \label{before_and_after_gym}
\end{figure}

\subsection{Topic modeling}

For extracting the topics from the collection of descriptions, the technique uses \textit{Latent Dirichlet Allocation} (LDA)~\cite{lda}. LDA uses the statistical model created by \textit{Dirichlet} for discovering the topics that appear in the collection of descriptions. It is a non supervised algorithm since it doesn't require labeled data to work. The idea is that a topic is a collection of words that frequently appear together.

As an example, if we feed LDA with documents talking about traveling and navigation it will group together words like \emph{map}, \emph{traffic}, \emph{route} and \emph{position}, and will group together words like \emph{city}, \emph{attraction}, \emph{tour} and \emph{visit}. If we then provide as input the description of the \textit{\textbf{London Restaurants Bars \& Pubs +}} application, it will be classified as belonging to both groups, since the description contains words of both groups. It is important to emphasize that applications can belong to several topics. The output of LDA, beside the topics themselves, is the probability of a certain document belonging to a certain topic (see Table~\ref{lda_example_table}). The maximum number of topics a document can belong to is a parameter of the algorithm. In the original paper this number was 4, in this work I have used 5. Also, in the original paper the number of topics was 30, the same number of categories that are in the Google Play store.

The original implementation of CHABADA used the Mallet framework~\cite{mallet}, and I kept it for the new version as well.

\begin{table}[h!]
\centering
\begin{tabular}{@{}lllll@{}}
\toprule
\textbf{Application} & \textit{$topic_1$} & \textit{$topic_2$} & \textit{$topic_3$} & \textit{$topic_4$} \\ \midrule
\multicolumn{1}{r}{\textit{$app_1$}}      & 0.55              & 0.45              & --                 & --                 \\
\multicolumn{1}{r}{\textit{$app_2$}}      & --                 & --                 & 0.67              & 0.33              \\
\multicolumn{1}{r}{\textit{$app_3$}}      & 0.5               & 0.3               & --                 & 0.2               \\
\multicolumn{1}{r}{\textit{$app_4$}}      & --                 & --                 & 0.4               & 0.6               \\ \bottomrule
\end{tabular}
\caption{An example with 4 application and 4 topics with the probabilities of belonging to each.}
\label{lda_example_table}
\end{table}

\subsection{Clustering}

The result of topic modeling is a vector of affinity values to each topic for each application. The CHABADA technique needs groups of applications. For this reason, the applications need to be clustered in groups. The technique uses K-means for this task. K-means is one of the most widely used clustering algorithms~\cite{kmeans}.

K-means works by selecting a centroid for each cluster, and associates each element of the space with it's nearest centroid. The centroids can be moved by a small amount in a series of iterations for improving the clusters. The numbers of clusters is a parameter of the algorithm and must be fine-tuned. The are several ways of fine-tuning the number of clusters, the CHABADA technique uses a technique known as the silhouette~\cite{silhouette}. The silhouette is a measure of how closely related is an element to the elements of its cluster and how loosely related is to the elements of the other clusters. The value of the silhouette can vary from -1 to 1. -1 means that the number of clusters was poorly chosen, and 1 that the number of clusters is adequate to the shape of the data. 

\subsection{API analysis}

Once the clusters of applications are identified, the technique can search for outliers in the clusters based on the behavior of the application implementation. For extracting this behavior information the technique relies on static analysis. This kind of analysis has weaknesses like obfuscation, but in the case of the API calls to the Android framework, those need to be explicitly declared.

The static analysis iterates over the bytecode of the application and extracts the list of API calls. This list of API calls includes all the calls to the methods to the Android framework. There are certain parts of the API that are key to the normal functionality of all apps, like the application life cycle or the UI subsystem. Including these API calls in the features of the machine learning algorithms would introduce noise and cause overfitting. CHABADA disregards any API call that is not considered sensitive. Sensitive means that it is associated to a certain permission. It relies on the work of Felt et al to extract the mapping between permissions and API calls~\cite{felt}. Since the permission related to the API calls is available, the analysis ignores also the API calls which associated permission is not declared. If the permission is not declared in the manifest of the application, the API call will fail to execute, so is not relevant to the analysis of the behavior of the application.

\subsection{Outlier detection}

% SVM
In the original paper the outlier detection was performed using {\it One-Class Support Vector Machine\/}~(OC-SVM)~\cite{svm}. This technique is used to learn the features of \emph{one class} of elements. This kind of machine learning algorithm performs well when the dataset has many elements of one class at not many of other classes. One can provide only samples of one class and the classifier will be able to identify samples belonging to the same class. The sensitive API calls are considered as a set of binary features, and each cluster is trained using a subset of the elements of said cluster. This way, the cluster has a model of what API calls are common in the cluster. This model can then be used to identify applications that are outliers. The outliers are identified by the distance from the hyperplane created by the OC-SVM as the model. The bigger the distance the more different is an element from the common behavior. A rank of outliers can be obtained by ranking the distances of the elements to the hyperplane.

% KNN
In further work of CHABADA~\cite{Kuznetsov:CHABADABookChapter:ASD:2015} the outlier detection system was changed to a distance based approach. This technique is based on calculating the distance between elements in a vector space. One way of for classifying the outliers is taking the elements with a greater distance to it's neighbors as outliers. This method is known as {\it k-Nearest Neighbors\/} (kNN). This algorithm needs a parameter k that determines how many neighbors are checked. In~\cite{Kuznetsov:CHABADABookChapter:ASD:2015} the number used as parameter was 5, that was chosen as a good trade-off between the two extremes. A very small number of neighbors is too sensitive to noise and a very high number will be useless as it will practically classify everything as an outlier.

The outlier score is taken by the average of the distances. An application can be considered an outlier because it uses API calls that have never been used in the cluster or that are rarely used. Since the decision of being an outlier or not is a binary decision, the score needs a threshold that determines from what score value an application is considered an outlier within the cluster or not. An easy approach to do this is to sort the dataset by score and return a certain percentage of it with the highest score. Other approach is to use quartile statistics. The potential outliers using this statistical method would be those applications that exceed the third quartile by at least 1.5 times the \emph{interquartile range} ($Q_3 - Q_1$).

The approach used in the paper was originally proposed by Kriegel et al.~\cite{krieger} based on transforming the scores into probabilities using \emph{Gaussian scaling}. Given the mean $\mu$ and the standard deviation $\sigma$, it uses the Gaussian error function $erf()$ to turn the score into a probability:

\begin{align}
  P(s) = \max \big\{ 0, \text{erf}\big(\frac{s-\mu}{\sqrt{2}\sigma}\big) \big\}
\end{align}

All the applications with a probability not equal to 0 is considered an outlier.

%  LocalWords:  CHABADA GPS outliers outlier NLP preprocessing plai
%  LocalWords:  metadata dataset british chinese indian biclycle un
%  LocalWords:  london restaur applic everi bicycl distanc durat LDA
%  LocalWords:  navig lllll centroid centroids overfitting OC SVM
%  LocalWords:  hyperplane
