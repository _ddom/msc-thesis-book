\chapter{State of the art\label{sec:state_of_the_art}}

This thesis is a major extension of the CHABADA technique, which I
explain in more details in the next chapter. While the CHABADA
technique has been the first to generally check app descriptions
against app behavior, it builds on a history of previous work
combining natural language processing and software development.

\section{Mining App Descriptions}

Most related to CHABADA is the WHYPER framework of Pandita
et~al.~\cite{pandita:whyper:usenix:2013}.  Just like CHABADA, WHYPER
attempts to automate the risk assessment of Android apps, and applies
natural language processing to app descriptions.  The aim of WHYPER is
to tell whether the need for \emph{sensitive permissions} (such as
accesses to contacts or calendar) is motivated in the application
description.  In contrast to CHABADA, which fully automatically learns
which topics are associated with which APIs (and by extension, which
permissions), WHYPER requires manual annotation of sentences
describing the need for permissions.  Also, CHABADA goes beyond
permissions in two ways: first, it focuses on APIs, which provide a
more detailed view, and it aims for general mismatches between
expectations and implementations.
    
The very idea of app store mining was introduced one year earlier when
Harman et al.\ mined the Blackberry app
store~\cite{harman:app-store-mining:msr:2012}.  They focused on app
meta-data to find patterns such as a correlation consumer rating and
the rank of app downloads, but would not download or analyze the apps
themselves.

The characterization of ``normal'' behavior comes from mining related
applications; in general, CHABADA assumes what most applications in a
well-maintained store do is also what most users would expect to be
legitimate.  In contrast, recent work by Lin et
al.~\cite{lin:mental:ubicomp:2012} suggests \emph{crowdsourcing} to
infer what users expect from specific privacy settings; Lin et al.\
also highlight that privacy expectations vary between app categories.
Such information from users can well complement what I infer from app
descriptions.



\subsection{Behavior/Description Mismatches}

The CHABADA approach is also related to techniques that apply natural
language processing to infer specifications from comments and
documentation.  Lin Tan et al.~\cite{tan:icomment:sosp:2007} extract
implicit program rules from program corpora and use these rules to
automatically detect inconsistencies between comments and source code,
indicating either bugs or bad comments.  Rules apply to ordering and
nesting of calls and resource accesses (``$f_a$ must not be called
from $f_b$'').
    
H\o{}st and \O{}stvold~\cite{host:debugging:ecoop:2009} learn from
program corpora which verbs and phrases would normally be associated
with specific method calls, and used these to identify misnamed
methods.
    
Pandita et al.~\cite{pandita:inferring:icse:2012} identify sentences
that describe code contracts from more than 2,500 sentences of API
documents; these contracts can be checked either through tests or
static analysis.
    
All these approaches compare program code against formal program
documentation, whose semi-formal nature makes it easier to extract
requirements.  In contrast, CHABADA works on end-user documentation,
which is decoupled from the program structure.


\subsection{Detecting Malicious Apps}

There is a large body of industrial products and research prototypes
that focus on identifying known malicious behavior. Most influential
for the CHABADA technique was the paper by Zhou and
Jiang~\cite{Zhou:DissectingMalware:OAKLAND:2012}, who use the
permissions requested by applications as a filter to identify
potentially malicious applications; the actual detection uses static
analysis to compare sequences of API calls against those of
\emph{known} malware.  In contrast to all these approaches, CHABADA
identifies outliers even without knowing what makes malicious
behavior.
    
The TAINTDROID system~\cite{enck:taintdroid:osdi:2010} tracks dynamic
information flow within Android apps and thus can detect usages of
sensitive information.  Using such dynamic flow information would
yield far more precise behavior insights than static API usage;
similarly, profilers such as
ProfileDroid~\cite{Wei:ProfileDroid:MobiCom:2012} would provide better
information; however, both TAINTDROID and ProfileDroid require a
representative set of executions.  Integrating such techniques in
CHABADA, combined with automated test
generation~\cite{Hu:GUItestingAndroid:AST:2011,Yang:GrayBoxTestAndroid:FASE:2013,Machiry:Dynodroid:FSE:2013,Amalfitano:AndroidGuitar:ASE:2012},
would allow to learn normal and abnormal patterns of information flow,
which has been implemented and published by the same research group
of CHABADA~\cite{Avdiienko:MUDFLOW:ICSE:2015}.


