.PHONY: book clobber clean wc comments todo xxx

LTX=pdflatex
BIB=bibtex
GLO=makeglossaries

BOOKNAME=msc_thesis.pdf

RMIF=rm -f *.alg \
	*.acr \
	*.acn \
	*.lof \
	*.log \
	*.lot \
	*.out \
	*.bak \
	*.toc \
	*.xdy \
	*.ist \
	*.gls \
	*.glo \
	*.blg \
	*.aux \
	*.bbl \
	*.glg \
	*.glsdefs \
	main.run.xml \
	main-blx.bib

book: clobber
	$(LTX) main.tex
	$(BIB) main
	$(GLO) main
	$(LTX) main.tex
	$(LTX) main.tex
	mv main.pdf $(BOOKNAME)

clobber: clean
	rm -f *.pdf

clean:
	$(RMIF)
	cd src/ && $(RMIF)

$(BOOKNAME): book

wc: $(BOOKNAME)
	@echo "Number of words:"
	@pdftotext $(BOOKNAME) - | wc -w

todo:
	@grep -n 'TODO: ' **/*.tex *.tex

xxx:
	@grep -n 'XXX: ' **/*.tex *.tex

comments: todo xxx
