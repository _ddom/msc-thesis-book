# Goddamm thesis book

Issues are open, so we can track them inside bitbucket.

To build the book do `make` or `make book`. To cleanup intermediate files do `make clean` and to remove EVERYTHING generated do `make clobber`.

To see the number of words do `make wc`.

Do `make todo` and `make xxx` to see the ToDos and the notes. `make comments` to see both.

## Current status

I'm writing a draft of the implementation chapter. Lots of rambling and typos ahead. Ideas first, pretty text latter.


